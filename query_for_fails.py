import requests
import json
# filepath = 'jenkins_master'
filename = 'quarantine.txt'

def main():
    request_for_fails = requests.get('http://rest:5000/quarantine/tests')
    failed_tests_json = request_for_fails.json()
    with open(filename, 'a') as f:
        for uuid in failed_tests_json['failed_test_uuids']:
            f.write('--include ' + uuid + '\n')

if __name__ == "__main__":
  main()
