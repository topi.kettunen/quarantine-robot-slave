FROM registry.gitlab.com/topi.kettunen/robot-slave

ENV NAME=quarantine-slave
ENV LABELS="quarantine"

COPY query_for_fails.py query_for_fails.py

RUN chmod ugo+x query_for_fails.py
